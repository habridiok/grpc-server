package com.gdn.grpc.server.service;

import com.gdn.grpc.server.entity.dto.ProductDTO;
import com.gdn.grpc.server.repository.ProductRepositoryService;
import generatedproto.Product;
import generatedproto.ProductServiceGrpc;
import io.grpc.stub.StreamObserver;

import java.util.List;
import java.util.stream.Collectors;

public class ProductService extends ProductServiceGrpc.ProductServiceImplBase {

  private ProductRepositoryService productRepositoryService = new ProductRepositoryService();

  @Override
  public void createProduct(Product.CreateProductRequest request,
      StreamObserver<Product.ProductResponse> responseStreamObserver) {
    ProductDTO productDTO = productRepositoryService.saveProduct(request);
    responseStreamObserver.onNext(this.convertToGrpcProductResponse(productDTO));
    responseStreamObserver.onCompleted();
  }

  @Override
  public void updateProduct(Product.UpdateProductRequest request,
      StreamObserver<Product.ProductResponse> responseStreamObserver) {
    ProductDTO productDTO = productRepositoryService.updateProduct(request);
    responseStreamObserver.onNext(this.convertToGrpcProductResponse(productDTO));
    responseStreamObserver.onCompleted();
  }

  @Override
  public void getAllProduct(Product.GetAllProductRequest request,
      StreamObserver<Product.GetAllProductResponse> responseStreamObserver) {
    List<Product.ProductResponse> productResponses =
        convertToGrpcProductResponses(productRepositoryService.getAllProduct());
    responseStreamObserver.onNext(this.convertToGrpcGetAllProductResponse(productResponses));
    responseStreamObserver.onCompleted();
  }

  @Override
  public void getBySku(Product.SkuRequest request,
      StreamObserver<Product.ProductResponse> responseStreamObserver) {
    ProductDTO productDTO = productRepositoryService.getProductBySku(request);
    responseStreamObserver.onNext(this.convertToGrpcProductResponse(productDTO));
    responseStreamObserver.onCompleted();
  }

  private Product.GetAllProductResponse convertToGrpcGetAllProductResponse(
      List<Product.ProductResponse> productResponses) {
    return Product.GetAllProductResponse.newBuilder().addAllProductResponse(productResponses)
        .build();
  }

  private List<Product.ProductResponse> convertToGrpcProductResponses(
      List<ProductDTO> productDTOS) {
    return productDTOS.stream().map(this::convertToGrpcProductResponse)
        .collect(Collectors.toList());
  }

  private Product.ProductResponse convertToGrpcProductResponse(ProductDTO productDTO) {
    return Product.ProductResponse.newBuilder().setId(productDTO.getId())
        .setSku(productDTO.getSku()).setProductname(productDTO.getProductName())
        .setPrice(productDTO.getPrice()).setStock(productDTO.getStock()).build();
  }

}
