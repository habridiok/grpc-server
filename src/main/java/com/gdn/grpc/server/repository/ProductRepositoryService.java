package com.gdn.grpc.server.repository;

import com.gdn.grpc.server.entity.ProductEntity;
import com.gdn.grpc.server.entity.dto.ProductDTO;
import generatedproto.Product;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;
import java.util.stream.Collectors;

public class ProductRepositoryService {

  private DozerBeanMapper mapper = new DozerBeanMapper();

  public ProductDTO saveProduct(Product.CreateProductRequest request) {
    ProductEntity product = this.convertCreateRequstToEntity(request);
    this.save(product);
    return mapper.map(product, ProductDTO.class);
  }

  public ProductDTO updateProduct(Product.UpdateProductRequest request) {
    ProductEntity product = this.convertUpdateRequstToEntity(request);
    this.update(product);
    return mapper.map(product, ProductDTO.class);
  }

  public ProductDTO getProductBySku(Product.SkuRequest request) {
    ProductEntity product = this.getBySku(request.getSku());
    return mapper.map(product, ProductDTO.class);
  }

  public List<ProductDTO> getAllProduct() {
    return this.getAll().stream().map(this::convertEntityToDTO).collect(Collectors.toList());
  }

  private Session createSession() {
    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
    Session session = sessionFactory.openSession();
    session.beginTransaction();
    return session;
  }

  private void save(ProductEntity product) {
    Session session = this.createSession();
    session.save(product);
    session.getTransaction().commit();
    session.close();
  }

  private void update(ProductEntity productEntity) {
    Session session = this.createSession();
    session.update(productEntity);
    session.getTransaction().commit();
    session.close();
  }

  private List<ProductEntity> getAll() {
    Session session = this.createSession();
    List<ProductEntity> result = session.createQuery("from ProductEntity").list();
    session.getTransaction().commit();
    session.close();
    return result;
  }

  private ProductEntity getBySku(String sku) {
    Session session = this.createSession();
    Query query = session.createQuery("from ProductEntity p where p.sku=:sku");
    query.setParameter("sku", sku);
    ProductEntity productEntity = (ProductEntity) query.uniqueResult();
    session.getTransaction().commit();
    session.close();
    return productEntity;
  }

  private ProductEntity convertCreateRequstToEntity(Product.CreateProductRequest request) {
    return ProductEntity.builder().productName(request.getProductname()).sku(request.getSku())
        .price(request.getPrice()).stock(request.getStock()).build();
  }

  private ProductEntity convertUpdateRequstToEntity(Product.UpdateProductRequest request) {
    return ProductEntity.builder().id(request.getId()).productName(request.getProductname())
        .sku(request.getSku()).price(request.getPrice()).stock(request.getStock()).build();
  }

  private ProductDTO convertEntityToDTO(ProductEntity product) {
    return mapper.map(product, ProductDTO.class);
  }

}
