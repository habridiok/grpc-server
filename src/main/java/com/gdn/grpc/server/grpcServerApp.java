package com.gdn.grpc.server;

import com.gdn.grpc.server.service.ProductService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class grpcServerApp {

  public static void main(String[] args) throws IOException, InterruptedException {
    Server server = ServerBuilder.forPort(9999).addService(new ProductService()).build();
    server.start();
    System.out.println("Server start at port " + server.getPort());
    server.awaitTermination();
  }
}
