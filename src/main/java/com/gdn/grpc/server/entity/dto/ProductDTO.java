package com.gdn.grpc.server.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {

  private long id;
  private String sku;
  private String productName;
  private int stock;
  private long price;

}
